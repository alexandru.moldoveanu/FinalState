Status:<br>
[![CI-Build](https://github.com/AlexandruMoldoveanu/FinalState/actions/workflows/build.yml/badge.svg)](https://github.com/AlexandruMoldoveanu/FinalState/actions/workflows/build.yml)<br>
1.Pregatirea configuratiei.<br>
2.Configurare runner.<br>
3.Testare reusita pentru un Hello Word<br>
4.Implementare script pentru Interfata<br>
5.Rezolvat problema clonatului(daca proiectul era deja clonat dadea eroare)<br>
6.Adaugarea parserului pentru erori/warninguri<br>
7.Adaptarea meniului<br>
8.Adaptare scripturi pe workflow<br>
9.Setare runner de git sa fie service<br>
10.Adaugare script pentru parsarea erorilor si trimiterea lor in prometheus<br>
11.Configurat prometheus si Grafana[TBD]<br>
12.Colectat metricile din prometeus in Grafana[TBD]<br>
13.Rezolvat interfata din Grafana[TBD]<br>
14.Setat jobul de nightly,sa ruleze in fiecare seara si sa stranga metricile[TBD]<br>

Tooluri:<br>
-github<br>
-git bash<br>
-prometeus<br>
-Grafana<br>

Scripturi:<br>
-Python<br>
-yaml<br>

<br>
